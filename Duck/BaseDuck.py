from abc import ABC, abstractmethod

class Duck(ABC): # isinstance to help to check if any unimplmented ducks
    def __init__(self):
        self.fly = None # dont need to implement if cannot fly. simulate interface concept

    @abstractmethod
    def name(self):
        pass
