# try flywithname and flynoname concrete fly interface implemention with this duck. So we do not need two classes.

from . import Duck
from .Fly import *  # get all fly interface


class FlyDuck(Duck):

    def __init__(self, flybehavior = flynoname):
        self.fly = lambda: flybehavior(self) # if the stragety is defined and then pass the strategy with the self to gain access to the class data

    def name(self): # name is the common thing across all ducks. they all need a name so it is an abstruct method
        print("I am a wing duck")

    def set_fly(self, flybehavior):
        self.fly = lambda: flybehavior(self)