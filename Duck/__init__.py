
# define the common namespace here
# each behavior can be done using different functions. obj got the chance to change the funtion during the runtime even.
# composite more tan inheritance
# we can use factory method to generate different ducks and use stragety method to give flexiblity to each different ducks


print("This is the strategy pattern")
print("DuckFactory(ducktype, stratgey) will generate the duck")
print("Duck obj.set_fly() can change the fly behavior. There are currently two fly behavior in Fly namespace flywithname() and flynoname() ")
#from modules.Duck.Fly import *  # get fly interface
from modules.Duck.BaseDuck import Duck  # get duck interface
from importlib import import_module
# this should be the place abstraction is the dominant so there are all abstract classes

def DuckFactory(duckType, *args, **kwargs):
    try:
        DuckModule = import_module("."+duckType, package="modules.Duck")
        DuckClass = getattr(DuckModule, duckType)
        DuckInstance = DuckClass(*args, **kwargs)
    except (AttributeError, ModuleNotFoundError):
        raise ImportError('{} is not part of our pizza collection!'.format(duckType))
    else:
        if not issubclass(DuckClass, Duck):
            raise ImportError(
                "We currently don't have {}, but you are welcome to send in the request for it!".format(DuckClass))
    return DuckInstance