from abc import ABC, abstractmethod


class Pizza(ABC):
    def __init__(self):
        pass

    @abstractmethod
    def get_name(self):
        pass

    @abstractmethod
    def get_cost(self):
        pass




