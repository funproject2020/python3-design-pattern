from .BasePizza import Pizza


class VeggiePizza(Pizza):

    def __init__(self):
        self.name = "VeggiePizza"
        self.cost = 1
        Pizza.__init__(self)

    def get_name(self):
        print("this is {}".format(self.name))
        return self.name

    def get_cost(self):
        print("this costs {}".format(self.cost))
        return self.cost