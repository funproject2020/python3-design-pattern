# it is supposed to be directory initilization routing.
# we borrow this as the factory class. the factory class will generate the product as needed

# https://www.bnmetrics.com/blog/factory-pattern-in-python3-simple-version
# good reference

from importlib import import_module
from modules.PizzaFactory.BasePizza import Pizza   # used for isinstance check
print("this is the factory pattern")

def PizzaFactory(PizzaType, *args, **kwargs): # this method can be contractor for the pizza objects
    try:
        PizzaModule = import_module("."+PizzaType, package="modules.PizzaFactory")
        PizzaClass = getattr(PizzaModule, PizzaType)
        PizzaInstance = PizzaClass(*args, **kwargs)
    except (AttributeError, ModuleNotFoundError):
        raise ImportError('{} is not part of our pizza collection!'.format(PizzaType))
    else:
        if not issubclass(PizzaClass, Pizza):
            raise ImportError(
                "We currently don't have {}, but you are welcome to send in the request for it!".format(PizzaClass))
    return PizzaInstance